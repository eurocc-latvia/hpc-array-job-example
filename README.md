# Summary

> **Comment for creators:** First paragraph contains 200-400 word summary of the case, its general purpose and contents, role of the demonstration for the intended users (companies, institutions). This can be copied directly from the respective Google document where *Abstract* for each case was already prepared. However, note the last sentence with the reference to *EuroCC* project which everyone should use verbatim.

This example considers simple computational fluid dynamics (CFD) problem within an HPC environment: in order to calculate buoyancy-driven air flow in a heated room, *OpenFOAM* calculations are performed. However, high-performance computing (HPC) environment offers possibilities not available on individual workstations: parametric studies, i.e., large number of calculations can be launched and processed using arrays of HPC tasks. This is a very common feature for almost every HPC workload manager (*Slurm*, *Torqueue*) which saves time for the user by reducing time spent running each calculation case manually. This example also serves as a short, but thorough, summary of all the workflow stages required when working in an HPC environment: loading software modules, submitting tasks for the workload manager, using post-processing tools (Python), considers the use of containerized software as an alternative to pre-installed software modules.

**This HPC demonstration was prepared as a part of the training and dissemination activities organized by the [EuroCC National HPC Competence Centre of Latvia](https://eurocc-latvia.lv/?lang=en).**

# Table of contents

[[_TOC_]]




# Problem background

> **Comment for creators:** This section contains description of the physical problem on which the HPC example is built. Note that this section does not consider HPC aspects, but rather explains the purpose of this case is other than being an HPC demo-case. *For this simple example this description is rather short. Please feel free to expand it according to the complexity of your example. Links to external resources (e.g. scientific articles) are encouraged.*

This case considers a rectangular room with a cube-shaped heater — a general model that can be applied to various environments, e.g. industrial ovens, drying kilns, saunas. Depending on the temperature of the heater, airflow with various intensity will develop and can be studied via numerical simulation to fine tune the design. To study this case, *OpenFOAM* simulation for a buoyancy driven turbulent flow and radiation heat transfer is used. Further reading on this particular *OpenFOAM* example can be [found here](http://www.tfd.chalmers.se/~hani/kurser/OS_CFD_2009/AlexeyVdovin/Radiation_in_OpenFoam_final.pdf).

Walls of the room, as well as surface of the heater, has fixed temperature boundary conditions. Below is an example of the result for a case with heater surface temperature of *T*=900K: Geometry of the room and heater are shown with lines, but temperature field and airflow velocity is shown for a single diagonal plane.

![Python plot output](README_img/img_paraview.jpg)

However, this example considers the problem of performing large number of calculations, each with a different temperature boundary condition for the heater surface (*T* in range from 400K to 900K with a step of 10K). Post processing to show the velocity and temperature for each case as a GIF animation is also included see one of the frames below).

![Python plot output](README_img/img_fieldplot.jpg)



# Contents of this case

> **Comment for creators:** All demo-cases should contain description of the provided files, at least the base level of the file tree.

Here is the overview of all files in this repository. The contents and use of these files will be described in detail further in this document.
* README_img — Folder containing images for this *readme* file.
* `hotRadiationRoom_Base` — Basic case definition
  * `0` — Initial conditions for all the physical fields, including the fixed temperature boundary condition for the heater.
  * `constant` — Material properties and other physical constants.
  * `system` — Definitions of solvers, boundary conditions and other properties to obtain *OpenFOAM* solution.
  * `Allclean` — Script for removing old solutions present in the case folder.
  * `Allrun` — Script for running the OpenFOAM simulation stages (mesh generation, obtaining solution, `.vtk` file export, *OpenFOAM* postprocessing script).
* `hotRadiationRoom_ParaVIEW_state.pvsm` — [*ParaVIEW*](https://www.paraview.org/) state file prepared for easier examination of the produced `.vtk` files. This is done for user convenience, but most likely cannot be used in the HPC environment. Instead, VTK files should be copied off from the HPC and used on a local workstation where graphical user interface is present and *ParaVIEW* is installed.
* `hotRadiationRoom_Post_1.py` — Post processing script for generating `.png` and `.gif` images for a given plane in the room domain (plots of temperature and velocity).
* `hotRadiationRoom_Post_2.py` — Post processing script for generating a `.pdf` plot of air temperature and velocity for two points in the room (just above furnace and by the ceiling in the middle of the room).
* `hotRadiationRoom_SlurmJob` — Batch script for submitting array job to an HPC cluster where [*Slurm*](https://slurm.schedmd.com/sbatch.html) workload manager is installed.
* `README.md` — This *readme* file.

# Software prerequisites

> **Comment for creators:** Assumption here is that the case will be used in an HPC environment, not a personal computer. Therefore, long description of software installs is not required. However, this section should also point out alternative ways of launching this case, if applicable.

## Running the case on a pre-installed HPC cluster

* ***Git*** — To obtain this case directly from *GitLab* via the `clone` command. Alternatively, code can be downloaded as `.zip` or `.tar` archive and copied to the HPC cluster by other means.
* ***OpenFOAM 6.0*** — Used for the main simulation. If not available on the HPC cluster directly, *Singularity* container can be used, see notes below.
* ***Slurm*** or ***Moab*** — Used for orchestrating the simulation array. Please consult the documentation/administrator of your computer cluster in case other workload manager is being used.
* ***Python*** — Specifically, `pandas`, `matplotlib`, `numpy`, `imageio` and `glob` are used for generating the `.png` images in the post-processing stage. If not available on the HPC cluster directly (including software modules of scientific *Python* such as [*Anaconda*](https://www.anaconda.com/products/individual)), *Singularity* container can be used, see notes below.
* ***ParaVIEW*** (optional) — Can be used for examining the result `.vtk` files locally, after copying them off of the HPC cluster. Installers for your workstation can be found [here](https://www.paraview.org/download/).

## Using *Singularity* containers
As alternative to pre-installed software, [*Singularity*](https://github.com/sylabs/singularity/releases) containers can be used. Most likely your HPC cluster has Singularity software already installed — please contact your HCP admin if it is not. Please see this [article](https://modinst.lu.lv/en/singularity-containers/) as well as the [user guide](https://sylabs.io/guides/3.7/user-guide/) provided by Sylabs for more information about this tool. In short: Containers will allow you to use software that would otherwise require *root* privilege to install. Many containers for various scientific software is readily available for download from container repositories. Here are links to *Singularity* containers relevant for this example:
* [*OpenFOAM 6.0* container](https://hub.docker.com/r/openfoam/openfoam6-paraview54)
* [*Anaconda Python* container](https://hub.docker.com/r/continuumio/anaconda3)

Below is a short example for using a *Singularity* container to execute the post-processing *Python* script described further in this document. Similarly, *OpenFOAM* container can be used to execute the `Allrun` script. However, please read this readme file thoroughly, before attempting to implement these changes.

```
# Try to execute the provided post-processing script
# However, if the base OS does not feature scientific Python packages
# this will most likely fail and will not produce the result.
python hotRadiationRoom_Post.py

# Load 'singularity' module, most likely available on the HPC cluster you are using
module load singularity/3.4.1

# Obtain container using the link seen on DockerHub
# 'pull' will download the container within few seconds
singularity pull ~/my_anaconda.simg docker://continuumio/anaconda3

# Use the downloaded container to execute the post processing script
singularity exec ~/my_anaconda.simg python hotRadiationRoom_Post.py
```



# Obtaining the case

Download this case directly from GitLab by executing the command below. Alternatively, code can be downloaded as `.zip` or `.tar` archive and copied to the HPC cluster by other means. Then use `cd` to enter the directory of the case:
```
git clone https://gitlab.com/eurocc-latvia/example-repository.git
cd example-repository
```



# Running the case as *Slurm* or *Moab* array job

> **Comment for creators:** For your demo-case this chapter might be called differently, but the essence stays: it should describe the sequence of operations that the user must perform to launch the case. Or alternatively (as in this example below), what happens automatically when the case is launched as an array job.

To run the case using *Slurm*, submit the batch job script by executing the command as seen below. 
```
sbatch hotRadiationRoom_SlurmJob
```

In the case when *Moab* job scheduler is used - e.g., on Riga Technical University (RTU) cluster - the following command should be executed:
```
qsub hotRadiationRoom_MoabJob
```

Subsections below describe contents of the submitted *Slurm* job and what happens when this job is being executed. Note that these descriptions are for parts of the `hotRadiationRoom_SlurmJob` file and their purpose.

## *Slurm* script contents — Header
At the beginning of the `hotRadiationRoom_SlurmJob` file are several lines that consider the resources allocated for this job. Because we use `--array=400-900:10`, fifty separate jobs will be initiated, each with a different ID (`400, 410, .., 500`). This number will be used to specify the temperature of the heater surface. Each array job will allocate a single CPU core (`--ntasks=1`).
```
#SBATCH --job-name=hotRadiationRoomArray # Job name
#SBATCH --partition=regular              # Partition name
#SBATCH --ntasks=1                       # Number of tasks
#SBATCH --time=00:30:00                  # Time limit, hrs:min:sec
#SBATCH --output=array_%A-%a.log         # Standard output and error log
#SBATCH --array=400-900:10               # Array range (temperature values for this example)
```

## *Slurm* script contents — Loading software modules
Further down in the `hotRadiationRoom_SlurmJob` loading of several software components are specified, see the section *Software prerequisites* above). These lines will need to be edited if your HPC cluster has different titles of the software modules.
```
module purge
module load anaconda3/anaconda-2020.11
module load openfoam/openfoam-6.0
source /software/openfoam/OpenFOAM-6.0/etc/bashrc
```

## *Slurm* script contents — Copying the base case for each array job instance
Further commands will ensure that each array job will copy the base directory and replace the variable `furnaceTemp` with the appropriate array task (which is conveniently chosen to represent out studied temperature range).
```
cp -r ${SLURM_SUBMIT_DIR}/hotRadiationRoom_Base ${SLURM_SUBMIT_DIR}/hotRadiationRoom_T=${SLURM_ARRAY_TASK_ID}K

sed -Ei "s/(furnaceTemp *).*/\1 ${SLURM_ARRAY_TASK_ID};/" ${SLURM_SUBMIT_DIR}/hotRadiationRoom_T=${SLURM_ARRAY_TASK_ID}K/0/T
```

I.e., the line with `cp ..` creates a copy of the `hotRadiationRoom_Base` directory and gives it a unique title using the `SLURM_ARRAY_TASK_ID` which in this case is used as the temperature value. The command beginning with `sed ..` replaces a line in the `/0/T` file for the newly created case directory, setting the temperature boundary condition according to the `SLURM_ARRAY_TASK_ID`.

## *Slurm* script contents — Calling the main application

Further down in the `hotRadiationRoom_SlurmJob` file, the `Allrun` script is called and *OpenFOAM* simulation is started.
```
${SLURM_SUBMIT_DIR}/hotRadiationRoom_T=${SLURM_ARRAY_TASK_ID}K/Allrun
```

By examining the contents of the `Allrun` file, we see that all stages of the simulation is called here: Mesh generation with `blockMesh`, simulation with appropriate `$application` (which in this case is `buoyantSimpleFoam` as defined in `system/controlDict`). After the calculation is completed, `.vtk` files are generated with `foamToVTK` and `postProcess` is called to output velocity and temperature values for a predefined plane in the geometry.
```
runApplication blockMesh
runApplication $application
runApplication foamToVTK -latestTime
runApplication postProcess -func planeAnalysis -latestTime
```

Each of the *Slurm* array jobs will run for about 10-20 minutes depending on the performance of the available CPU cores. Several folders `hotRadiationRoom_T=400K .. hotRadiationRoom_T=900K` will be produced. Each case contains folder `VTK` where `.vtk` file is located for the final solution.




# Post-processing and visualization

> **Comment for creators:** In this example, post-processing is described separately as it is performed outside the defined HPC batch job. I urge you to do structure your *readme* similarly: To define separate sections for sets of activities that can be performed separately.

Download the `.vtk` files from the HPC cluster to interactively examine the solution using the *ParaVIEW* utility. `hotRadiationRoom_ParaVIEW_state.pvsm` is a *ParaVIEW* state file that aids this process (temperature and air movement velocity is visualized for a single slice of the room domain).

Additionally, the `postProcess` utility ensures that data for temperature and velocity on a predefined plane is exported for each array case: `postPorcessing/planeAnalysis/<latestTime>/T_vertical_plane.raw` (temperature) and `../U_vertical_plane.raw` (velocity). These files can be visualized using *Python*. Launch the following command to obtain `.png` plots and a combined `.gif` animation for every case (temperature value):
```
python `hotRadiationRoom_Post_1.py`.
```

Similarly, a script for creating a line plot of the air velocity and air temperature vs. furnace temperature can be created by running the second post-processing script:
```
python `hotRadiationRoom_Post_2.py`.
```

![Python plot output](README_img/img_pointplot.jpg)




# Suggestions for altering the case

> **Comment for creators:** For this simple example, the description here is rather short. However, please give all the necesarry information for the user to understand the steps and actions required to alter the case. More importantly: give *examples* on the aspects that the user *might want* to change.

* Change the temperature values and range by editing the array parameters in `hotRadiationRoom_SlurmJob`. Note that other physical parameters can be selected for replacement: `sed` command (*stream editor*) in the `hotRadiationRoom_SlurmJob` simply replaces a line in the *OpenFOAM* files. This solution can be easily edited to serve purposes of a different study (e.g. study various air densities).
* Change the room shape for all cases by editing `hotRadiationRoom_Base/system/blockMeshDict` where room and furnace geometry are defined by specifying point coordinates.
* Change the plane where `postProcess` utility will output the result. Plane definition is contained within the `hotRadiationRoom_Base/system/planeAnalysis` file.
