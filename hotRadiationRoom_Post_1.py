# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 17:58:18 2021

@author: Kristaps Bergfelds (kristaps.bergfelds@.lu.lv)
"""


#%% Package import
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from matplotlib.patches import Rectangle
import numpy as np
from glob import glob



#%% Data import

def importData(casepath):
    
    #  Reading VELOCITY data
    df_U = pd.read_csv(
            casepath + "U_vertical_plane.raw",
            sep = " ",
            comment = "#",
            names = ["x", "y", "z", "U_x", "U_y", "U_z"]
            )
    
    # Reading TEMPERATURE data
    df_T = pd.read_csv(
            casepath + "T_vertical_plane.raw",
            sep = " ",
            comment = "#",
            names = ["x", "y", "z", "T"]
            )
    
    # Joining both dataframes into one
    df = pd.merge(df_U,df_T, how="inner")
    
    # Data processing (velocity magnitude)
    df["U_avg"] = np.sqrt(df["U_x"]**2+df["U_y"]**2+df["U_z"]**2)
    
    return df



#%% Data interpolation and plotting (for contour plotting)

def plotData(dataframe):
    
    # Interpolation grid size
    n_grid_x = 100
    n_grid_y = 20
    
    # Converting to Numpy arrays for more stable operation
    x = dataframe["x"].to_numpy()
    y = dataframe["z"].to_numpy()
    T = dataframe["T"].to_numpy()
    U = dataframe["U_avg"].to_numpy()
    
    # Creating interpolation functions for parameters
    xi = np.linspace(x.min(), x.max(), n_grid_x)
    yi = np.linspace(y.min(), y.max(), n_grid_y)
    triang = tri.Triangulation(x, y)
    interpolator_T = tri.LinearTriInterpolator(triang, T)
    interpolator_U = tri.LinearTriInterpolator(triang, U)
    
    # Preparing arrays for plotting (using interpolation functions)
    Xi, Yi = np.meshgrid(xi, yi)
    Ti = interpolator_T(Xi, Yi)
    Ui = interpolator_U(Xi, Yi)
    
    # Defining subplots
    fig, (axis_top, axis_bot) = plt.subplots(
        nrows = 2,
        ncols = 1,
        figsize = (11,5),
        dpi = 150
    )
    
    # Creating space between subplots
    plt.subplots_adjust(hspace = 0.3)
    #plt.style.use('dark_background')
    
    # Setting common parameters for both subplots
    for ax in (axis_top, axis_bot):
        ax.set_aspect(1.0)
        ax.set_xlim((x.min(),x.max()))
        ax.set_ylim((y.min(),y.max()))
        ax.set_xlabel("$x$, $\mathrm{m}$")
        ax.set_ylabel("$y$, $\mathrm{m}$")
        ax.add_patch(
            Rectangle(
                (0.6, 0.0),
                0.7,
                0.5,
                fc ='gray',
                ec ='black',
                lw = 0.5
                )
            )
    
    # Plotting TEMPERATURE
    axis_top.set_title('Temperature')
    templevels = np.linspace(300-273, 360-273, 21)
    contours = axis_top.contourf(xi, yi, Ti-273.0, levels=templevels, cmap="coolwarm")
    fig.colorbar(
        contours,
        ax = axis_top,
        label = "$T$, $\mathrm{°C}$"
        )
    
    # Plotting VELOCITY
    axis_bot.set_title('Velocity magnitude')
    vellevels = np.linspace(0, 1.1, 11)
    contours = axis_bot.contourf(xi, yi, Ui, levels=vellevels, cmap="viridis")
    fig.colorbar(
        contours,
        ax = axis_bot,
        label = "$|\\vec{U}|$, $\mathrm{\\frac{m}{s}}$"
        )
    
    return fig



#%%  Constructing dictionary with paths and studied parameter values

caseslist = glob("hotRadiationRoom_T*")
relatpath = "/postProcessing/planeAnalysis/"

casesdicts = []

for case in caseslist:
    tempval = case.split("=")[1][:-1] # Extracting temperature digits
    path = glob(case + relatpath + "*")[0] + "/"
    casedict = {"path":path, "tempval":int(tempval)}
    casesdicts.append(casedict)



#%% Main program loop

for case in casesdicts:
    path = case["path"]
    print("Processing: {}".format(path))
    tempval = case["tempval"]
    
    dataframe = importData(path)
    figure = plotData(dataframe)
    
    figure.suptitle(
        'Furnace temperature {} °C'.format(tempval-270),
        fontsize = 18,
        x = 0.45
        )
    
    figure.savefig("field_plots_{}.png".format(tempval))



#%% Create GIF from images

import imageio

filenames = sorted(glob("*.png"), key=str.lower)
images = []

print("Loading images for GIF creation...")
# Gif to go forwards...
for filename in filenames:
    images.append(imageio.imread(filename))

# Gif to go backwards...
for filename in filenames[::-1]:
    images.append(imageio.imread(filename))

print("Saving GIF...")
imageio.mimsave("field_plots.gif", images, duration=1/24)