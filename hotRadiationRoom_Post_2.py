# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 17:58:18 2021

@author: Kristaps Bergfelds (kristaps.bergfelds@.lu.lv)
"""


#%% Package import
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from matplotlib.patches import Rectangle
import numpy as np
from glob import glob



#%% Data import

def importData(casepath):
    
    #  Reading VELOCITY data
    df_U = pd.read_csv(
            casepath + "U_vertical_plane.raw",
            sep = " ",
            comment = "#",
            names = ["x", "y", "z", "U_x", "U_y", "U_z"]
            )
    
    # Reading TEMPERATURE data
    df_T = pd.read_csv(
            casepath + "T_vertical_plane.raw",
            sep = " ",
            comment = "#",
            names = ["x", "y", "z", "T"]
            )
    
    # Joining both dataframes into one
    df = pd.merge(df_U,df_T, how="inner")
    
    # Data processing (velocity magnitude)
    df["U_avg"] = np.sqrt(df["U_x"]**2+df["U_y"]**2+df["U_z"]**2)
    
    return df



#%% Data interpolation and plotting (for contour plotting)

def returnPointData(dataframe, point):
    
    # Converting to Numpy arrays for more stable operation
    x = dataframe["x"].to_numpy()
    y = dataframe["z"].to_numpy()
    T = dataframe["T"].to_numpy()
    U = dataframe["U_avg"].to_numpy()
    
    # Creating interpolation functions for parameters
    triang = tri.Triangulation(x, y)
    interpolator_T = tri.LinearTriInterpolator(triang, T)
    interpolator_U = tri.LinearTriInterpolator(triang, U)
    
    # Getting point data from interpolation functions
    X, Y = point
    pointdata_T = interpolator_T(X, Y)
    pointdata_U = interpolator_U(X, Y)
    
    # Interpolation function returns masked arrays, hence .mean() to get value
    return pointdata_T.mean(), pointdata_U.mean()



#%%  Constructing dictionary with paths and studied parameter values

caseslist = glob("hotRadiationRoom_T*")
relatpath = "/postProcessing/planeAnalysis/"

casesdicts = []

for case in caseslist:
    tempval = case.split("=")[1][:-1] # Extracting temperature digits
    path = glob(case + relatpath + "*")[0] + "/"
    casedict = {"path":path, "tempval":int(tempval)}
    casesdicts.append(casedict)



#%% Getting point data for the FIRST POINT (ABOVE FURNACE)

# Define point where T and U values for all cases should be plotted
point = (1.0, 1.0)

# Define empty dataframe where to aggregate values from all cases
point_df_1 = pd.DataFrame(columns=['Furnace temperature', 'T', 'U'])

# Iterate through all cases and populate the dataframe with values
for case in casesdicts:
    path = case["path"]
    print("Processing: {}".format(path))
    tempval = case["tempval"]
    
    field_df = importData(path)
    point_data_T, point_data_U = returnPointData(field_df, point)
    point_df_1 = point_df_1.append(
        {
            'Furnace temperature':tempval,
            'T':point_data_T,
            'U':point_data_U
            },
        ignore_index = True
        )


#%% Getting point data for the SECOND POINT (CEILING)

# Define point where T and U values for all cases should be plotted
point = (4.0, 1.8)

# Define empty dataframe where to aggregate values from all cases
point_df_2 = pd.DataFrame(columns=['Furnace temperature', 'T', 'U'])

# Iterate through all cases and populate the dataframe with values
for case in casesdicts:
    path = case["path"]
    print("Processing: {}".format(path))
    tempval = case["tempval"]
    
    field_df = importData(path)
    point_data_T, point_data_U = returnPointData(field_df, point)
    point_df_2 = point_df_2.append(
        {
            'Furnace temperature':tempval,
            'T':point_data_T,
            'U':point_data_U
            },
        ignore_index = True
        )



#%% Plotting the point data

# Defining figure with two subfigures
fig, (axis_top, axis_bot) = plt.subplots(
    nrows = 2,
    ncols = 1,
    figsize = (11,5),
    dpi = 150
)

fig.subplots_adjust(hspace=0.4)

# Setting commong parameters for both subplots
for axis in (axis_top, axis_bot):
    axis.grid(b=True,axis="both", which = "major", linestyle='-', lw = 1.0)
    axis.grid(b=True,axis="both", which = "minor", linestyle='--', lw = 0.5)
    axis.minorticks_on()
    axis.set_xlabel(r"Furnace temperature, $\mathrm{°C}$")
    axis.set_xlim(
        (
            point_df_1['Furnace temperature'].min(),
            point_df_1['Furnace temperature'].max()
            )
        )

# Plotting data and setting unique parameters for subplots (axis title)
# Temperature
axis_top.plot(point_df_1['Furnace temperature'], point_df_1['T']-273.0, label = "Above furnace")
axis_top.plot(point_df_2['Furnace temperature'], point_df_2['T']-273.0, label = "Ceiling")
axis_top.set_ylabel(r"Air temperature ($T$), $\mathrm{°C}$")
axis_top.legend()
#Velocity
axis_bot.plot(point_df_1['Furnace temperature'], point_df_1['U'], label = 'Above Furnace')
axis_bot.plot(point_df_2['Furnace temperature'], point_df_2['U'], label = 'Ceiling')
axis_bot.set_ylabel(r"Air velocity ($U$), $\frac{m}{s}$")
axis_bot.legend()

# Setting global plot title
fig.suptitle(
    r'Air temperature and velocity at $x$={}m, $y$={}m'.format(
        point[0],
        point[1]
        )
    ,fontsize = 16
    )

# Saving figure
plt.savefig("point_plot.pdf")